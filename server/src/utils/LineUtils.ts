/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import {
	Position,
	Range,
} from "vscode-languageserver";

import { ILine } from "../assembler/KickAssembler";

export default class LineUtils {


	public static getLines(sourcelines:string[]):ILine[]|undefined {
		var lines = [];
		var next = 0;
		
		/**
		 * something
		 */
		var scope = 0;	//	something
		var last = []
		for(var i = 0; i < sourcelines.length; i++) {

			var source = this.removeComments(sourcelines[i]); 

			if (source) {

				//	search for { - add to scope
				if (source.indexOf("{") >= 0) {
					next += 1;
					last.push(scope);
					scope = next;
				}
				//	search for } - remove from scope
				if (source.indexOf("}") >= 0) {
					scope = last.pop();
				}
			}

			//	add new line info object
			var newline = <ILine> {};
			newline.number = i+1;
			newline.scope = scope;
			lines.push(newline);
		}
		return lines;
	}

	/**
	 * Given a line, returns what is the assume symbol/label/value at a specific position
	 */
	public static getTokenAtPosition(line:string, character:number):string|undefined {
		if (character < line.length) {
			const targetRegex = new RegExp("^.{0," + character + "}\\b([\\w.]*)\\b.*$");
			const targetMatch = line.match(targetRegex);
			if (targetMatch && targetMatch[1]) {
				return targetMatch[1];
			}
		}

		return undefined;
	}

	public static getTokenAtSourcePosition(sourceLines:string[]|undefined, line:number, column:number):string|undefined {
		if (sourceLines && sourceLines.length > line) {
			// Find the char and the surrounding symbol it relates to
			const sourceLine = LineUtils.removeComments(sourceLines[line]);
			return LineUtils.getTokenAtLinePosition(sourceLine, column);
		}

		return undefined;
	}

	public static getDirectiveAtPosition(line:string, character:number):string|undefined {
		if (character < line.length) {
			const targetRegex = new RegExp("^.{0," + character + "}\\b\\.|([\\w.]*)\\b.*$");
			const targetMatch = line.match(targetRegex);
			if (targetMatch && targetMatch[1]) {
				return targetMatch[1];
			}
		}
		return undefined;
	}

	public static getTokenAtLinePosition(sourceLine:string|undefined, column:number):string|undefined {
		if (sourceLine && column <= sourceLine.length) {
			let targetRegex = new RegExp("^.{0," + Math.max(column, 0) + "}\\b([\\w.]*)\\b.*$");
			let targetMatch = sourceLine.match(targetRegex);
			if (!targetMatch || !targetMatch[1]) {
				// Fallback: this regex is more lenient, so we can have rename working from the end of the string...
				// but it may give false positives
				targetRegex = new RegExp("^.{0," + Math.max(column - 1, 0) + "}\\b([\\w.]*)\\b.*$");
				targetMatch = sourceLine.match(targetRegex);
			}

			if (targetMatch && targetMatch[1]) {
				return targetMatch[1];
			}
		}

		return undefined;
	}	

	public static getTokenAtLinePosition2(sourceLine:string|undefined, column:number):string|undefined {
		if (sourceLine && column <= sourceLine.length) {
			let targetRegex = new RegExp("^[#$]{0," + Math.max(column, 0) + "}\\b([\\w.]*)\\b.*$");
			let targetMatch = sourceLine.match(targetRegex);
			if (!targetMatch || !targetMatch[1]) {
				// Fallback: this regex is more lenient, so we can have rename working from the end of the string...
				// but it may give false positives
				targetRegex = new RegExp("^[#$]{0," + Math.max(column - 1, 0) + "}\\b([\\w.]*)\\b.*$");
				targetMatch = sourceLine.match(targetRegex);
			}

			if (targetMatch && targetMatch[1]) {
				return targetMatch[1];
			}
		}

		return undefined;
	}	

	/**
	 * Given a line and a token, returns the location in that line (start and end) that the token is in
	 * A `character` parameter can be used when the token needs to be in that position
	 */
	public static getTokenPosition(line:string, token:string, character:number = -1):{start:number, end:number, length:number}|undefined {
		const len = token.length;
		let pos = line.indexOf(token);
		while (pos > -1) {
			if (character < 0 || (pos <= character && pos + len >= character)) {
				return { start: pos, end: pos + len, length: len };
			}
			pos = line.indexOf(token, pos + len);
		}
	}

	/**
	 * Same as getTokenPosition(), but returning a range of a specific line
	 */
	public static getTokenRange(line:string, token:string, lineNumber:number, character:number = -1):Range|undefined {
		const pos = LineUtils.getTokenPosition(line, token, character);
		if (pos) {
			return Range.create(Position.create(lineNumber, pos.start), Position.create(lineNumber, pos.end));
		}
	}

	/**
	 * Returns a line without comments
	 */
	public static removeComments(line:string):string|undefined {
		const removeCommentsRegex = /^(.+?)(;.+|)$/;
		const sourceLineNoCommentsMatch = line.match(removeCommentsRegex);
		if (sourceLineNoCommentsMatch && sourceLineNoCommentsMatch[1]) {
			return sourceLineNoCommentsMatch[1];
		}
	}

	/**
	 * Find beginning of Comment lines
	 * Second line.
	 * @param lines - an array of lines to look scan
	 * @param start - the starting postition to look from
	 */
	public static findCommentStart(lines:string[], start:number):number|undefined {
		var ret = -1;
		var line = start;
		var hasComment:boolean = true;

		//	some really bad code to get comments above directives
		for (line; line > 0; line--) {
			var src = lines[line].trim();
			if (src != "") {
				if (src.indexOf("/*") >= 0) {
					ret = line;
					break;
				}	
				if (src.indexOf("\\*") < 0) {
					hasComment = false;
				}
				if(src.indexOf("*") < 0) {
					hasComment = false;
				}	
			}
		}
		//console.log(line);
		return ret;
	}
}
