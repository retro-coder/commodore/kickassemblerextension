/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import { IConnection, TextDocument, TextDocumentIdentifier } from "vscode-languageserver";

//import { IAssemblerResult, ILine } from "./Assembler";
import { ISettings } from "./SettingsProvider";
import { IAssemblerResults, ILine } from "../assembler/KickAssembler";
import { ISymbol } from "../project/Project";

export interface IProjectInfoProvider {
    //getResults:() => IAssemblerResult|undefined;
    getSource:() => string[]|undefined;
    getSettings:() => ISettings;
    getLines:(textDocument:TextDocumentIdentifier) => ILine[]|undefined;
    getAssemblerResults:() => IAssemblerResults|undefined;
    getSymbols:() => ISymbol[]|undefined;
}

/*
    Base Class for All Providers 

    A Provider is an Object that is used to return
    information to the VsCode application. This
    information is used to do things like hover 
    support, compiling, etc.

*/
export class Provider {

    private _connection:IConnection;
    private _projectInfoProvider:IProjectInfoProvider;

    constructor(connection:IConnection, projectInfoProvider:IProjectInfoProvider) {
        this._connection = connection;
        this._projectInfoProvider = projectInfoProvider;
    }

    public getConnection():IConnection {
        return this._connection;
    }

    public getProjectInfo():IProjectInfoProvider {
        return this._projectInfoProvider;
    }
}